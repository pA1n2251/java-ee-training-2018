<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Upload book cover</title>
  </head>
  <body>
  <form enctype="multipart/form-data">
      <label for="id">Id:</label>
      <input type="number" name="id" id="id">
      <br>
      <input type="file" name="file" accept="image/jpeg">
      <input type="submit" value="Upload" formaction="/upload" formmethod="post">
      <input type="submit" value="Back" formaction="/" formmethod="get">
  </form>
  </body>
</html>
