package by.gsu.epamlab.filters;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Logger;

@WebFilter(filterName = "UploadBookCoverAndLogFilter", servletNames = "UploadBookCoverController")
@MultipartConfig
public class UploadBookCoverAndLogFilter implements Filter {
    private Logger log = Logger.getLogger(by.gsu.epamlab.controllers.UploadBookCoverController.class.getName());

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        log.info("Content type - " + request.getContentType());
        log.info("Protocol - " + request.getProtocol());
        log.info("Character encoding - " + request.getCharacterEncoding());
        log.info("Remote address - " + request.getRemoteAddr());
        String fileName = Paths.get(request.getPart("file").getSubmittedFileName()).getFileName().toString();
        Properties properties = new Properties();
        String realPath = req.getServletContext().getRealPath("/");
        log.info(realPath);
        try (FileInputStream fileInputStream =
                     new FileInputStream(realPath + "/WEB-INF/classes/by/gsu/epamlab/fileExtensions.properties")) {
            properties.load(fileInputStream);
            boolean anyMatch = properties.stringPropertyNames().stream()
                    .anyMatch(name -> fileName.contains(properties.getProperty(name)));
            if (!anyMatch) {
                HttpServletResponse httpResponse = (HttpServletResponse) resp;
                httpResponse.sendError(415);
                return;
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
