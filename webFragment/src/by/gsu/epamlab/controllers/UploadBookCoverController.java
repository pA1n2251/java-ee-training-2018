package by.gsu.epamlab.controllers;

import by.gsu.epamlab.model.file.FileHelper;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(name = "UploadBookCoverController", urlPatterns = "/upload")
@MultipartConfig
public class UploadBookCoverController extends HttpServlet {
    private final static Logger log =
            Logger.getLogger(by.gsu.epamlab.controllers.UploadBookCoverController.class.getName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.info("This is book cover update");
        int id = Integer.parseInt(request.getParameter("id"));
        Part filePart = request.getPart("file");
        String realPath = getServletConfig().getServletContext().getRealPath(File.separator);
        FileHelper.upload(id, filePart, realPath);
        response.sendRedirect("/");
    }
}
