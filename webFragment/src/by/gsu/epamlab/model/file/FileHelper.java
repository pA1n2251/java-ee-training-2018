package by.gsu.epamlab.model.file;

import by.gsu.epamlab.model.dbase.DBConnection;

import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class FileHelper {
    private static final String UPDATE_COVER = "UPDATE `books` SET `bookCover`= ? WHERE `bookId` = ?";

    public static void upload(int bookId, Part filePart, String realPath) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            InputStream fileContent;
            fileContent = filePart.getInputStream();
            byte[] buffer = new byte[fileContent.available()];
            fileContent.read(buffer);
            File fileDir = new File(realPath + File.separator + "Covers");
            fileDir.mkdir();
            File targetFile = new File(realPath + File.separator + "Covers" + File.separator + fileName);
            OutputStream outStream = new FileOutputStream(targetFile);
            outStream.write(buffer);
            outStream.close();
            fileContent.close();

            connection = DBConnection.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_COVER);
            preparedStatement.setString(1, fileName);
            preparedStatement.setInt(2, bookId);
            preparedStatement.executeUpdate();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException(e.toString(), e);
        } finally {
            DBConnection.closeConnection(connection, preparedStatement, resultSet);
        }
    }

}
