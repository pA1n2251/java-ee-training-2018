package by.gsu.epamlab.model.dbase;

import java.sql.*;

public class DBConnection {
    public static Connection getConnection() {
        try {
            Class.forName("org.gjt.mm.mysql.Driver");
            return DriverManager.getConnection("jdbc:mysql://localhost:3306/epam", "epam","111");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void closeConnection(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            System.err.println("Can't close connection." + e);
        }
    }

}
