package by.gsu.epamlab.listeners;

import by.gsu.epamlab.ifaces.IBookDAO;
import by.gsu.epamlab.model.factories.BookFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.logging.Logger;

@WebListener
public class BaseInfoLoggingApplicationContextListener implements ServletContextListener {

    private final static Logger log =
            Logger.getLogger(BaseInfoLoggingApplicationContextListener.class.getName());

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        IBookDAO iBookDAO = BookFactory.getClassFromFactory();
        servletContextEvent.getServletContext().setAttribute("DAO", iBookDAO);
        log.info("Server name and version - " + servletContext.getServerInfo());
        log.info("Real path - " + servletContext.getRealPath("/"));
        log.info("Wab app name - " + servletContext.getServletContextName());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
