package by.gsu.epamlab.controllers;

import by.gsu.epamlab.ifaces.IBookDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddBookController", urlPatterns = "/add")
public class AddBookController extends HttpServlet {

    private IBookDAO iBookDAO;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        iBookDAO = (IBookDAO) config.getServletContext().getAttribute("DAO");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String author = request.getParameter("author");
        String description = request.getParameter("description");
        iBookDAO.addBook(name, author, description);
        response.sendRedirect("/");
    }

}
