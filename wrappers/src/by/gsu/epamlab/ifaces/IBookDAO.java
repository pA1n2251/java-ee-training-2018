package by.gsu.epamlab.ifaces;

import by.gsu.epamlab.model.beans.Book;

import java.awt.*;
import java.util.List;

public interface IBookDAO {
    List<Book> getBooks();
    void addBook(String name, String author, String description);
    void updateBook(String author, String description, Long bookId);
}
