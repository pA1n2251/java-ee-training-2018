<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Servelt Basics</title>
  </head>
  <body>
    <h1>This is simple app to retrieve books from database</h1>
    <table border="1">
      <c:forEach var="book" items="${books}">
        <tr>
          <td>${book.name}</td>
          <td>${book.author}</td>
        </tr>
      </c:forEach>
    </table>
    <form method="get">
        <label for="second">Second</label>
        <input type="number" name="second" id="second">
        <input type="submit" value="Make server unavailable" name="unavailFlag" formaction="/">
    </form>
  </body>
</html>
