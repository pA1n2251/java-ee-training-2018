package by.gsu.epamlab.controllers;

import by.gsu.epamlab.ifaces.IBookDAO;
import by.gsu.epamlab.model.beans.Book;
import by.gsu.epamlab.model.factories.BookFactory;
import by.gsu.epamlab.model.impl.DBBookImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@WebServlet(name = "BookController", urlPatterns = "/")
public class BookController extends HttpServlet {

    private final static Logger log =
            Logger.getLogger(by.gsu.epamlab.controllers.BookController.class.getName());
    private final String KEY_BOOKS = "books";
    private IBookDAO iBookDAO;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        iBookDAO = (IBookDAO) config.getServletContext().getAttribute("DAO");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("unavailFlag")!=null){
            log.info("Start block");
            int second = Integer.parseInt(request.getParameter("second"));
            throw new UnavailableException("Servlet unavailable during" + second + "second", second);
        }
        List<Book> books = iBookDAO.getBooks();
        request.setAttribute(KEY_BOOKS, books);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/main.jsp");
        rd.forward(request, response);
    }
}
