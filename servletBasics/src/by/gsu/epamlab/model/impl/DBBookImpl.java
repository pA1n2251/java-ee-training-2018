package by.gsu.epamlab.model.impl;

import by.gsu.epamlab.ifaces.IBookDAO;
import by.gsu.epamlab.model.beans.Book;
import by.gsu.epamlab.model.dbase.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBBookImpl implements IBookDAO {

    private final String GET_BOOKS = "SELECT * FROM `books`";
    private final String BOOK_ID = "bookId";
    private final String BOOK_NAME = "name";
    private final String BOOK_AUTHOR = "author";

    public List<Book> getBooks(){
        List<Book> books = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DBConnection.getConnection();
            preparedStatement = connection.prepareStatement(GET_BOOKS);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                books.add(new Book(resultSet.getLong(BOOK_ID), resultSet.getString(BOOK_NAME), resultSet.getString(BOOK_AUTHOR)));
            }
            return books;
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        } finally {
            DBConnection.closeConnection(connection, preparedStatement, resultSet);
        }
    }
}
