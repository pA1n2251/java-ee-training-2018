package by.gsu.epamlab.ifaces;

import by.gsu.epamlab.model.beans.Book;

import java.util.List;

public interface IBookDAO {
    List<Book> getBooks();
}
