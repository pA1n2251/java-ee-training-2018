package by.gsu.epamlab.controllers;

import by.gsu.epamlab.ifaces.IBookDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DeleteBookController", urlPatterns = "/delete")
public class DeleteBookController extends HttpServlet {
    private IBookDAO iBookDAO;

    @Override
    public void init() {
        iBookDAO = (IBookDAO) getServletContext().getAttribute("DAO");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long bookId = Long.parseLong(request.getParameter("id")) ;
        iBookDAO.deleteBook(bookId);
        response.sendRedirect("/");
    }
}
