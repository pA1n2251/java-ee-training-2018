package by.gsu.epamlab.listeners;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Date;
import java.util.logging.Logger;

@WebListener()
public class SessionListener implements HttpSessionListener{

    private final static Logger log =
            Logger.getLogger(SessionListener.class.getName());

    // Public constructor is required by servlet spec
    public SessionListener() {
    }

    public void sessionCreated(HttpSessionEvent se) {
        log.info("Session created - " + new Date(se.getSession().getCreationTime()).toString());
    }

    public void sessionDestroyed(HttpSessionEvent se) {
        log.info("Destroy time " + new Date());
    }

}
