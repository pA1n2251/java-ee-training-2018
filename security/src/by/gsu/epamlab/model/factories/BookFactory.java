package by.gsu.epamlab.model.factories;

import by.gsu.epamlab.ifaces.IBookDAO;
import by.gsu.epamlab.model.impl.DBBookImpl;

public class BookFactory {
    public static IBookDAO getClassFromFactory(){
        return new DBBookImpl();
    }
}
