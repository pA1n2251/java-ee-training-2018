<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Delete</title>
</head>
<body>
<form>
    <label for="id">Id:</label>
    <input type="number" name="id" id="id">
    <br>
    <input type="submit" value="Delete book" formaction="/delete" formmethod="post">
    <input type="submit" value="Back" formaction="/" formmethod="get">
</form>
</body>
</html>
