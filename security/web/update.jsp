<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update book</title>
</head>
<body>
<form action="/add">
    <label for="id">Id:</label>
    <input type="number" name="id" id="id">
    <br>
    <label for="author">Author:</label>
    <input type="text" name="author" id="author">
    <br>
    <label for="description">Description:</label>
    <input type="text" name="description" id="description">
    <br>
    <input type="submit" value="Update book" formaction="/update" formmethod="post">
    <input type="submit" value="Back" formaction="/" formmethod="get">
</form>
</body>
</html>
