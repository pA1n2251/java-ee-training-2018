package by.epam;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.util.Iterator;
import java.util.Map;

public class ListMapping extends BodyTagSupport {
    private Iterator<? extends Map.Entry<String, ? extends ServletRegistration>> entryIterator;

    @Override
    public int doStartTag() throws JspException {
        Map<String, ? extends ServletRegistration> registration = pageContext.getServletContext().getServletRegistrations();
        entryIterator = registration.entrySet().iterator();
        return addAttributes(EVAL_BODY_INCLUDE);

    }

    private int addAttributes(int returnState) {
        if (entryIterator.hasNext()) {
            Map.Entry<String, ? extends ServletRegistration> entry = entryIterator.next();
            pageContext.setAttribute("url", entry.getValue().getMappings());
            pageContext.setAttribute("servlet", entry.getKey() + "<br>");
            return returnState;
        } else {
            return SKIP_BODY;
        }
    }


    @Override
    public int doAfterBody() throws JspException {
        return addAttributes(EVAL_BODY_AGAIN);
    }
}
