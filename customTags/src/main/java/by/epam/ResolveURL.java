package by.epam;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.net.URL;

public class ResolveURL extends SimpleTagSupport {
    private String url;

    public ResolveURL() {
    }

    public ResolveURL(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void doTag() throws JspException, IOException {
        PageContext pageContext = (PageContext) getJspContext();
        String resource = pageContext.getServletContext().getRealPath(url);
        getJspContext().setAttribute("url", url);
        if (resource.isEmpty()) {
            getJspContext().setAttribute("resourсe", "Not found");
        } else {
            getJspContext().setAttribute("resourсe", resource);
        }
        getJspBody().invoke(null);
    }
}
