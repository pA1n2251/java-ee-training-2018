<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add book</title>
</head>
<body>
<form action="/add">
    <label for="name">Name:</label>
    <input type="text" name="name" id="name">
    <br>
    <label for="author">Author:</label>
    <input type="text" name="author" id="author">
    <br>
    <label for="description">Description:</label>
    <input type="text" name="description" id="description">
    <br>
    <input type="submit" value="Add book" formaction="/add" formmethod="post">
    <input type="submit" value="Back" formaction="/" formmethod="get">
</form>
</body>
</html>
