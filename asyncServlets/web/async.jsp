<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Async</title>
</head>
<body>
    <h1>Result async process</h1>
    <p> Result 1 = ${sum} - ${time1} ms</p>
    <p> Result 2 = ${mult} - ${time2} ms</p>
</body>
</html>
