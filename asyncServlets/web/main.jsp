<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Servelt Basics</title>
</head>
<body>
<h1>This is simple app to retrieve books from database</h1>
<form method="get">
    <table border="1">
        <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Author</td>
            <td>Description</td>
        </tr>
        <c:forEach var="book" items="${books}">
            <tr>
                <td>${book.bookId}</td>
                <td>${book.name}</td>
                <td>${book.author}</td>
                <td>${book.description}</td>
            </tr>
        </c:forEach>
    </table>
    <label for="second">Second</label>
    <input type="number" name="second" id="second">
    <input type="submit" value="Make server unavailable" name="unavailFlag" formaction="/">
    <input type="submit" value="Add book" formaction="add.jsp">
    <input type="submit" value="Update book" formaction="update.jsp">
    <input type="submit" value="Destroy session" formaction="/destroy">
</form>
<a href="/async">Do async </a>
</body>
</html>
