package by.gsu.epamlab.controllers;

import by.gsu.epamlab.ifaces.IBookDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(name = "UpdateBookController", urlPatterns = "/update")
public class UpdateBookController extends HttpServlet {

    private final static Logger log =
            Logger.getLogger(by.gsu.epamlab.controllers.UpdateBookController.class.getName());
    private IBookDAO iBookDAO;

    @Override
    public void init() {
        iBookDAO = (IBookDAO) getServletContext().getAttribute("DAO");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String author = request.getParameter("author");
        String description = request.getParameter("description");
        Long bookId = Long.parseLong(request.getParameter("id")) ;
        iBookDAO.updateBook(author, description, bookId);
        response.sendRedirect("/");
    }

}
