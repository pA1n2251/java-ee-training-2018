package by.gsu.epamlab.controllers;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(name = "AsyncFirstAttributeController", urlPatterns = "/async", asyncSupported = true)
public class AsyncFirstAttributeController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private final static Logger log =
            Logger.getLogger(by.gsu.epamlab.controllers.AsyncFirstAttributeController.class.getName());

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final long start = System.currentTimeMillis();
        request.setAttribute("startTime", start);
        final AsyncContext asyncContext = request.startAsync();
        asyncContext.start(() -> asyncContext.dispatch("/asyncSecond"));
        final int sum = 2 + 2 + 2 + 2;
        request.setAttribute("sum", sum);
        request.setAttribute("time1",(System.currentTimeMillis()-start));
    }
}
