package by.gsu.epamlab.controllers;

import javax.servlet.AsyncContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(name = "AsyncSecondAttributeController", urlPatterns = "/asyncSecond", asyncSupported = true)
public class AsyncSecondAttributeController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private final static Logger log =
            Logger.getLogger(by.gsu.epamlab.controllers.AsyncFirstAttributeController.class.getName());

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final long start = (long) request.getAttribute("startTime");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        final long mult = 2 * 2 * 2 * 2;
        request.setAttribute("mult", mult);
        request.setAttribute("time2", (System.currentTimeMillis()-start));
        RequestDispatcher rd = request.getRequestDispatcher("/async.jsp");
        rd.forward(request, response);
    }
}
